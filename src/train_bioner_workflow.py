 
 
'''
@author: ahmed allam
'''
 
import os
from collections import OrderedDict
import numpy as np
from pyseqlab.features_extraction import HOFeatureExtractor
from pyseqlab.ho_crf_ad import HOCRFAD, HOCRFADModelRepresentation
from pyseqlab.workflow import GenericTrainingWorkflow
from pyseqlab.utilities import ReaderWriter, DataFileParser,TemplateGenerator, \
                               create_directory, generate_datetime_str, generate_trained_model
from bioner_extractor import BioNERAttributeExtractor, POS_AttributeExtractor, CONLL00_SeqAttributeExtractor
 
current_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
dataset_dir = os.path.join(root_dir, "dataset")
labelers_dir = os.path.join(root_dir, 'labelers')

def read_datafiles(data_type):
    # initialize a data file parser
    dparser = DataFileParser()
    # provide the options to parser such as the header info, the separator between words and if the y label is already existing
    header = ('w', 'bio_ner')
    # y_ref is a boolean indicating if the label to predict is already found in the file
    y_ref = True
    # spearator between the words/observations
    column_sep = "\t"
    seqs = []
    if(data_type == 'train'):
        fpath = os.path.join(dataset_dir, "original", "training","Genia4ERtask1.iob2")
    elif(data_type == 'test'):
        fpath = os.path.join(dataset_dir, "original", "test","Genia4EReval1.iob2")
    for seq in dparser.read_file(fpath, header, y_ref=y_ref, column_sep = column_sep):
        seqs.append(seq)
    return(seqs)

def load_DataFileParser_options():
    data_parser_options = {}
    data_parser_options['header'] =  "main"
    data_parser_options['y_ref'] = True
    data_parser_options['column_sep'] = "\t"
    data_parser_options['seg_other_symbol'] = None
    return(data_parser_options)
        
def template_config():
    template_generator = TemplateGenerator()
    templateXY = {}
    # generating template for attr_name = w
    template_generator.generate_template_XY('w', ('1-gram:2-gram:3-gram', range(-2,3)), '1-state:2-states:3-states', templateXY)
    
    # generating template for the prefix and suffix
    attr_names = ('prefix', 'suffix')
    for attr_name in attr_names:
        for i in range(3, 6):
            template_generator.generate_template_XY('{}_{}'.format(attr_name, i), ('1-gram', range(0,1)), '1-state', templateXY)
   
    # generating templated for the orthographic features
    attr_names = ('initcap',
                  'capword',
                  'allcaps',
                  'capsmix',
                  'alphanumix',
                  'alphanum',
                  'upperchar',
                  'lowerchar',
                  'shortnum',
                  'int',
                  'real',
                  'roman',
                  'hasdash',
                  'initdash',
                  'enddash',
                  'punc',
                  'quote')
    for attr_name in attr_names:
        template_generator.generate_template_XY(attr_name, ('1-gram', range(0,1)), '1-state', templateXY)
 
    #generate template for word shape and degenerated shape
    attr_names = ('shape', 'shaped')
    for attr_name in attr_names:
        template_generator.generate_template_XY(attr_name, ('1-gram', range(0,1)), '1-state', templateXY)
    
    template_generator.generate_template_XY('pos', ('1-gram:2-gram', range(-1,2)), '1-state:2-states:3-states', templateXY)
    template_generator.generate_template_XY('chunk', ('1-gram:2-gram', range(-1,2)), '1-state:2-states:3-states', templateXY)
 
    templateY = {'Y':()}
 
    return(templateXY, templateY)
 
def run_training(optimization_options, template_config, num_seqs=np.inf, profile=False):
    import cProfile
    if(profile):
        local_def = {'optimization_options':optimization_options,
                     'template_config':template_config,
                     'num_seqs':num_seqs
                    }
        global_def = {'train_crfs':train_crfs}
        uid = generate_datetime_str()
        profiling_dir = create_directory('profiling', root_dir)
        cProfile.runctx('train_crfs(optimization_options, template_config, num_seqs=num_seqs)',
                        global_def, local_def, filename = os.path.join(profiling_dir, "timeprofile_{}".format(uid)))
    else:
        return(train_crfs(optimization_options, template_config, num_seqs=num_seqs))

    
def label_seqs_preprocessing(loadedseqs, data_type, labelers_info):
    """preprocess the sequences by assigning part-of-speech tag and chunk tag
    
       Args:
           loadedseqs: list of sequences of :class:`SequenceStruct`
           data_type: string ``train`` or ``test``
           labelers_info: ordered dictionary containing info about the labelers
       
       Example:
       
           ::
           
               labelers_info = {'tagger':{'model_dir': path to the pos tagger,
                                          'beam_size':integer (optional), specifying the size of the beam search 
                                          },
                                 'chunker':{'model_parts': path to the chunker
                                           'beam_size':integer (optional), specifying the size of the beam search
                                           } 
                               }
    
    """
    seqs_pred = []
    for labeler_name, labeler_info in labelers_info.items():
        model_dir = labeler_info['model_dir']
        if(labeler_name == 'tagger'):
            attr_extractor = POS_AttributeExtractor
            label_name = 'pos'
        elif(labeler_name == 'chunker'):
            attr_extractor = CONLL00_SeqAttributeExtractor
            label_name = 'chunk'
            
        labeler = revive_learnedmodel(model_dir, attr_extractor)
        beam_size = labeler_info.get('beam_size')
        labeler_dir = os.path.abspath(os.path.join(model_dir, os.pardir))
        print("using {} to label sequences".format(labeler_name))
        if(not seqs_pred):
            seqs_pred = labeler.decode_seqs("viterbi", labeler_dir, seqs = loadedseqs, beam_size=beam_size)
        else:
            upd_seqs= [seqs_pred[seq_id]['seq'] for seq_id in seqs_pred]
            seqs_pred = labeler.decode_seqs("viterbi", labeler_dir, seqs = upd_seqs, beam_size=beam_size)
 
        for seq_id in seqs_pred:
            seq = seqs_pred[seq_id]['seq']
            y_pred = seqs_pred[seq_id]['Y_pred']
            T = seq.T
            for t in range(1, T+1):
                seq.X[t][label_name] = y_pred[t-1]
            seq.seg_attr.clear()
            
    seqs = [seqs_pred[seq_id]['seq'] for seq_id in seqs_pred]
    target_dir = create_directory(dataset_dir, "processed")
    header = list(seqs[0].X[1].keys())
    header.append('bio_ner')
    write_seqs(seqs, os.path.join(target_dir, data_type + '.txt'), header)
 
def preprocess_data():
    # prepare the training data
    seqs = {}
    labelers_info = OrderedDict()
    labelers_info['tagger'] = {'model_dir':os.path.join(labelers_dir, '2017_1_8-0_9_48_894305')}
    labelers_info['chunker'] = {'model_dir':os.path.join(labelers_dir, '2016_12_21-16_37_6_147704')}
    for dtype in ('train', 'test'):
        seqs_raw = read_datafiles(dtype)
        seqs_processed = label_seqs_preprocessing(seqs_raw, dtype, labelers_info)
        seqs[dtype] = seqs_processed
    return(seqs)
    
def write_seqs(ref_seqs, out_file, header, sep = "\t"):
    ReaderWriter.log_progress("\t".join(header) + "\n", out_file)
    for i in range(len(ref_seqs)):
        ref_seq = ref_seqs[i]
        T = ref_seq.T
        line = ""
        for t in range(1, T+1):
            for field_name in ref_seq.X[t]:
                line += ref_seq.X[t][field_name] + sep
            if(ref_seq.flat_y):
                line += ref_seq.flat_y[t-1] + "\n"
            else:
                line += "\n"
        line += "\n"
        ReaderWriter.log_progress(line,out_file)

        
def restructure_output(in_file, out_file, positions):
    f_out = open(out_file, 'a')
    with open(in_file, 'r') as f:
        for line in f:
            line = line.rstrip("\n")
            if(line):
                elems = line.rsplit()
                new_elems = []
                for pos in positions:
                    new_elems.append(elems[pos])
                new_line = "\t".join(new_elems)
            else:
                new_line = ""
            f_out.write(new_line + "\n")
    f_out.close()
 
def revive_learnedmodel(model_dir, attr_extractor):
    modelparts_dir = os.path.join(model_dir, "model_parts")
    lmodel = generate_trained_model(modelparts_dir, attr_extractor)
    return(lmodel)
    
#     optimization_options = {"method" : "SAPO",
#                             "num_epochs":17,
#                             'update_type':'max-fast',
#                             'beam_size':5,
#                             'shuffle_seq':True,
#                             "tolerance":1e-6
#                             }
  
     

 
def train_crfs(optimization_options, template_config, num_seqs=np.inf):
    crf_model = HOCRFAD
    model_repr = HOCRFADModelRepresentation
    # init attribute extractor
    bioner_attrextractor = BioNERAttributeExtractor()
    # get the attribute description 
    attr_desc = bioner_attrextractor.attr_desc
    
    # load templates
    template_XY, template_Y = template_config()
    # init feature extractor
    fextractor = HOFeatureExtractor(template_XY, template_Y, attr_desc)
    # no need for feature filter
    fe_filter = None

    # create a root directory for processing the data
    wd = create_directory('wd', root_dir)
    workflow_trainer = GenericTrainingWorkflow(bioner_attrextractor, fextractor, fe_filter, 
                                               model_repr, crf_model,
                                               wd)
    

    # define the data split strategy
    # since data is already organized in separate files (train and test)
    # we use all data in training file 
    dsplit_options = {'method':"none"}
    # since we are going to train using perceptron-based method
    full_parsing = True

    # load data_parser_options
    data_parser_options = load_DataFileParser_options()
    # load train file -- for reproducibility purpose, we will use already processed file 
    train_file = os.path.join(dataset_dir, 'processed_original', "train.txt")
    data_split = workflow_trainer.seq_parsing_workflow(dsplit_options,
                                                       seq_file=train_file,
                                                       data_parser_options=data_parser_options,
                                                       num_seqs=num_seqs,
                                                       full_parsing = full_parsing)
    trainseqs_id = data_split[0]['train']
    crf_m = workflow_trainer.build_crf_model(trainseqs_id,
                                             "f_0", 
                                             full_parsing=full_parsing)
    model_dir = workflow_trainer.train_model(trainseqs_id, 
                                             crf_m, 
                                             optimization_options)
    use_options_seqsinfo = {'seqs_info':workflow_trainer.seqs_info,
                            'model_eval':False,
                            'file_name':'train_fold_0.txt',
                            'sep':'\t'
                           }
    
    model_train_perf = workflow_trainer.use_model(model_dir, use_options_seqsinfo)  
    # since we did not ask for computing performance, we will get empty dictionary
    print(model_train_perf)    
    # get test file
    test_file = os.path.join(dataset_dir, 'processed_original', "test.txt")
    # define the options to use the model while providing a sequence file
    use_options_seqfile = {'seq_file':test_file,
                           'data_parser_options':data_parser_options,
                           'num_seqs':num_seqs,
                           'model_eval':False,
                           'file_name':'test_fold_0.txt',
                           'sep':'\t'
                          }

    model_test_perf = workflow_trainer.use_model(model_dir, use_options_seqfile)    
    # since we did not ask for computing performance, we will get empty dictionary
    print(model_test_perf)
    return(model_dir)

if __name__ == "__main__":
    pass

    

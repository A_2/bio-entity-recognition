BioNER
##########

To illustrate the use of `PySeqLab package <https://bitbucket.org/A_2/pyseqlab>`__ in training a biomedical named entity recognizer (``BioNER``) using `BioNLP/NLPBA 2004 dataset <http://www.nactem.ac.uk/tsujii/GENIA/ERtask/report.html>`__.
Navigate to the ``tutorials`` folder to explore the notebooks for building, training and using ``BioNER`` tagger. A rendered html version of the tutorial is available on `PySeqLab applications page <http://pyseqlab.readthedocs.io/en/latest/applications.html>`__.